# Simplistic makefile to produce man pages from markup files

saturn.7.gz: README.md
	pandoc $< -s -t man | gzip -9 >$@

