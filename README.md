% SATURN(7) Version 1.0 | saturn.icl.utk.edu Users Manual
% Aurelien Bouteiller
% January 2018

NAME
====

Saturn - the _saturn.icl.utk.edu_ clusters, at the Innovative Computing Laboratory (ICL), The University of Tennessee.

SYNOPSYS
========

[TOC]

DESCRIPTION
===========

This page describes the features and architecture of the Saturn clusters, at the University of Tennessee.

The Saturn clusters are a set of small development clusters administered by the ICL admin staff and the DisCo team.

If your issue is not solved by reading this document, get [help](#markdown-header-help). Make sure you reviewed currently [known problems](#markdown-header-known-problems).

  Accounts
----------
To obtain your access to the Saturn clusters, fill-up the [account request form].

When registering your account, you will be given the opportunity to provide a preferred user name. Prefer using the same as your ICL account, or UTK Volnet account, if you have one.

Password authentication is not possible on Saturn, and multiple trials to login with the wrong credential will get your IP banned, so beware. Instead of a password, you will need to provide an SSH public key. It is recommended that you create a new public-private key pair (for security reasons), and that your key has a password. To create a SSH key pair, use the following command: `ssh-keygen -f .ssh/icl_saturn`

Copy the content of the `~/.ssh/icl_saturn.pub` into the request form. The public key should look like `ssh-rsa AAAAxyzxyz= user@host`. Do not send by email, in the form, or share the private key.

Once you get the confirmation that your account has been created, login using the following command `slogin myname@saturn.icl.utk.edu -i ~/.ssh/icl_saturn`

There are various ways of automating the selection of the key (see [ssh_config(5)]) or to login from multiple hosts without putting your private key at risk by transferring it to all these machines (see  [ssh-agent(1)]). We provide an example [ssh_config](https://bitbucket.org/icl/saturn-users/wiki/ssh_config) file snippet. This page also addresses the `too many authentication failures` error.

  Etiquette
-----------
The Saturn clusters are a shared resource. Please be mindful of other users and avoid being disruptive. Most of our policy is _good will_ based and trust our users' good manners. Remember that the Saturn home area is shared with NFS to the compute nodes, and therefore loosely secure. We advice against importing sensitive or confidential material on this system.

The headnode (Saturn itself) is not a compute resource. The headnode is for editing your files, compiling your programs, and launching mpirun/slurm commands. Only light duty processing (like visualization) shall take place there. **Do not run compute intensive, or disk intensive activities on the headnode.** Always run your compute tasks (including serial ones) on the compute nodes.

**Do not write on the NFS home areas from multiple nodes at the same time.** Use the [scratch disks](#markdown-header-filesystem) (local or shared) if you need to write a large volume of file I/O.

Do not reserve nodes in exclusive mode, except for performance measurements. Try to reserve nodes in exclusive mode for as short as possible. Try to schedule long-lasting and compute heavy activity overnight, when possible.


HARDWARE
========

The Saturn system is the headnode for the Saturn clusters: alembert, bezout, cauchy, and descartes.

  Network
---------
All machines are connected through a shared Ethernet network. Home areas are accesssible through NFS on all nodes. In addition, we have **three SEPARATE Infiniband** compute networks.

1. The alembert and bezout clusters are connected to the same Infiniband switch (100G EDR MSB7700/U1). **Note that they do not have the same type of Infiniband adapters**.
2. The cauchy cluster is served by a 10G SDR Infiniband switch (SilverStorm 9024).
3. The descartes cluster is served by a 20G DDR Infiniband switch (MT47396 Infiniscale-III Mellanox).

  alembert cluster
------------------
Nodes are named a00 to a08. Each node features:

+ HP ProLiant DL380 Gen9 chassis
+ 2x [Haswell E5-2650 v3] @ 2.30GHz. 20 cores, 40 ht
+ 64GB RAM
+ Infiniband EDR 100G (MT4115 cards), Ethernet
+ OCZ RevoDrive (v1.37) PCI-E SSD 50GB

  bezout cluster
----------------
Nodes are named b00 to b05. Each node features:

+ Dell T630 chassis
+ 2x [Haswell E5-2650 v3] @ 2.30GHz. 20 cores, 40 ht
+ 32GB RAM
+ Infiniband FDR 56G (MT4103 cards), Ethernet

  cauchy cluster
----------------
Nodes are named c00 to c15. Each node features:

+ Supermicro X8DA3 chassis
+ 2x [Westmere-EP E5606] @2.13GHz. 8 cores
+ 24G RAM
+ Infiniband SDR 10G (MT25208 cards), Ethernet

  descartes cluster
-------------------
Nodes are named d00 to d15. Each node features:

+ Dell T7500 chassis
+ 2x [Gainestown E5520] @2.27GHz. 8 cores, 16 ht
+ ~12G RAM (actual amount varies between 8G and 16G)
+ Infiniband DDR 20G (MT25208 cards), Ethernet

  GPUs & coprocessors
---------------------
The full list of resources can change too often for this page to keep-up. It is recommended that you deploy your compute jobs on the accelerated machines using [slurm constraints](#markdown-header-slurm). You may obtain the live list of available resources using the [saturnsh](#markdown-header-the-saturnsh-command) script. In broad strokes, we have the following accelerators available:

+ Some alembert machines host a single K20, K80, or P100 board.
+ Most of the bezout machines are heavily accelerated, with NV-K40, NV-GTX, AMD or MIC coprocessors.
+ 12 of the descartes machines form a GPU accelerated cluster with Tesla C2050 and IB20G.


SOFTWARE
========

The system uses Scientific Linux 7.x (at this time, 7.3). Scientific Linux is a derivative of Red Hat Enterprise Linux/CentOS 7. If you identify software that would benefit to most users and is missing, please contact us (especially if you want to have some RHEL package installed).

There is a lot of pre-installed optional software, most can be found in `/sw/` and `/spack/`.

  Environment modules
---------------------
In order to use the provided optional software, we recommend you use [module(1)] to locate and load optional software. Note that modules are also very convenient to maintain  your own set of software packages compiled in your home directory.

+ `module avail` lists available modules.
+ `module list` prints currently active modules (you should have CUDA, Open MPI and MKL loaded by default).
+ `module show packagename` shows the environment variables the module sets.
+ `module load use.own` lets you add your own modules, located in `$HOME/privatemodules`.

  Spack
-------
At this time, the use of Spack is experimental, and is not loaded in the default environment. You can load the pre-compiled Spacks by sourcing the Spack environment file `. /spack/share/spack/setup-env.sh`. This will add Spacks packages to your `module avail` list. You may add this to your `~/.bashrc` file.


FILESYSTEM
==========

  Home area
-----------
Your home area is located in `/home/$USER/`. It is NFS exported from the headnode to the compute nodes. Note that it is not the same home area as your ICL LDAP account. You can use [rsync(1)] to transfer files between Saturn and ICL or external resources.

Your home area keeps an history of your files with _snapshots_. If you erase or modify a file by mistake, you can retrieve the file by accessing the (hidden) snapshot directory. `ls ~/.zfs/snapshot/auto_hourly-2017-06-29-0314` will list the history of your files at that time (also weekly and monthly); **Snapshot dates are in UTC**, local time is in EST. Simply copy the historic file into your normal home directory to restore it yourself. Older monthly snapshots are kept in backups, and require administrative assistance to access.

Your home area has a quota (by default 10G). Your snapshots do not count toward your quota.

  Headnode shared scratch
-------------------------
If you need to store very large files, you also have access to `/scratch/shared/$USER/`. This filesystem is NFS exported from the headnode, and is available on all nodes. This is a network volume, writeable by everybody. Its content is cleared only when space gets scarce. For performance reasons, the NFS share is in _async_ mode, which means that writes from compute nodes may not be immediately visible to other hosts.

  Compute node local scratch
----------------------------
If your compute tasks generate (or read) a large volume of files, you may consider storing the files on local compute node scratch storage. 
The environment variable $TMPDIR contains the full path to this area area during a job allocation srun or sbatch . 
This is a local disk, writeable by only yourself. 
This directory is removed automatically at the end of the job allocation.

  Compute node local SSD
------------------------
Some compute nodes have an optional SSD drive (notably alembert nodes). When an SSD is available, the `/scratch/localssd/` is writeable by everybody. If the directory is not readable, the drive is not physically present.

Beware, its content may be wiped without notice. These are small drives, try to remember cleaning your files when you are done.

  Core Files
------------
By default, core files are not generated. The _ulimit_ value set on the headnode during an [salloc(1)], or in a [sbatch(1)] script will propagate to the compute nodes. Thus, you can enable core file generation on the compute nodes by performing a `ulimit -c unlimited` before executing the command of interest (presumably [mpirun(1)]) in your allocation.

On the compute nodes, your core file will be generated in `/scratch/local/cores/`. To reduce the NFS server load, these core files are generated on a compute node local filesystem. you can _gdb_ these files in-place on the compute nodes, or you can move your core files into `/scratch/shared/$USER/` (see [saturnsh](#markdown-header-the-saturnsh-command)).


SLURM
=====

Compute nodes are accessible only through a Slurm allocation. General information on using Slurm is widely available [slurm-quickstart][slurm(1)].

Considering that Saturn is a development machine, **on Saturn, a Slurm reservation is shared by default**, that is, the Slurm scheduler will try to allocate first the least loaded machines, but your allocated nodes may be oversubscribed and may execute the work of up to 8 users simultaneously.

Note that executing batch jobs on the _shared_ queue will result in heavily oversubscribing the machines, which is probably not what you wanted. **Send your batch jobs to the _batch_ queue.**

  Constraints
-------------
You can specify the features of the nodes you are allocated by setting node _constraints_. `salloc -N2 -n2 -Cib100g,cuda mpirun ~/ompi/imb/IMB-MPI1 pingpong` will run a ping pong benchmark on two randomly selected alembert nodes with an Nvidia GPU.

To list all available constraints, use `sinfo -el -o"%n %z %b"`

If you find inconsistent constraints (e.g. you requested a node with the _cuda_ constraint, but the machine has no GPU), please let the admins know.

You may use `salloc -N4 -wa00 -xa04 -Calembert` to make sure _a00_ is in your allocation, and _a04_ is not.

  Reservation duration
----------------------
The **default duration for a reservation is one hour**. You can change the reservation duration to up to 12 hours on the shared and batch queues. `salloc -t600 -Cdescartes foo` to run for 10 hours on a descartes machine.

  Exclusive Access
------------------
It is possible to make exclusive reservations for some nodes. Exclusive reservations are managed through the _exclusive_ and _batch_ queues. Please refrain from requesting exclusive access, except if you need to do performance measurements. descartes nodes are always available for interactive/debugging and cannot be reserved for exclusive access. Use [sinfo(1)] to obtain more information on queues.

+ The _exclusive_ queue is intended to run short duration, performance sensitive experiments. The maximum job runtime is 15 minutes. This queue is available 24/7. If you are not time constrained, prefer using the _batch_ queue, so as to let other users share the machines during work hours.

```salloc -N2 -n2 -Cib100g -pexclusive mpirun ~/ompi/imb/IMB-MPI1 pingpong```

+ The _batch_ queue is intended to run long duration, performance sensitive experiments, or batch jobs. The maximum job runtime is 12 hours. This queue is available only overnight (20:00 to 8:00). You may specify that your batch job is not exclusive by adding the parameter ```--oversubscribe``` to your [salloc(1)] or [sbatch(1)] command.

```
salloc -pbatch --reservation=batch hostname
  salloc: Requested reservation not usable now
  salloc: Pending job allocation 1317
  salloc: job 1317 queued and waiting for resources
```
```
sbatch -pbatch --reservation=batch testbatch.srun
  Submitted batch job 1318
squeue
  Thu Jun 29 11:52:37 2017
             JOBID PARTITION     NAME     USER    STATE       TIME TIME_LIMI  NODES NODELIST(REASON)
     1318_[0-15%4]     batch testbatc bouteill  PENDING       0:00      2:00      4 (Reservation)
```


THE SATURNSH COMMAND
====================

The _saturnsh_ command helps executing a simple shell command on all (or a subset of) the compute nodes. This command essentially creates a short-lived Slurm shared reservation, and then executes _pdsh_. Note that nodes that are currently reserved in exclusive mode will not execute the command.

+ To obtain the **list of active processes** (all users) at each node `saturnsh -p`
+ To print a **backtrace** of a running program `saturnsh -b myprogramname`
+ To **list all YOUR threads** at each node `saturnsh -u`
+ To **list available GPU accelerators** on the nodes `saturnsh -g`
+ Get the system utilization `saturnsh -v`
+ To execute an arbitrary command `saturnsh  mv /scratch/local/cores/core.myapp.* /scratch/shared/PUBLIC/cores/`; this command will **copy core files from the node local disk to the headnode shared scratch**
+ To restrict the command to a range of nodes `saturnsh -fc -r05:08 uname -a` shows the operating system version on nodes c05, c06, c07, c08; The `-f` option filters nodes according to the _pdsh_ range syntax.
+ To execute a pipeline `saturnsh 'ls | grep foo'`
+ To remove the pretty printed node names `saturnsh -s command`



HELP
====

  Known problems
----------------

+ I compiled my own Open MPI and using the BTL TCP deadlocks soon after start or when sending large messages.
    + Add `-mca btl_tcp_if_include eth0` to exclude the SCIF/virbr interfaces from the TCP ifaces used by the Open MPI TCP BTL.

+ When I do `srun -N2 -n2 mpi-program`, this results in launching two copies of a 1-node program.
    + ~~This is a limitation in the way Slurm and our Open MPI is compiled, and is under investigation.~~ The issue is fixed in the default Open MPI install. If you compile your own Open MPI, use `salloc -N2 -n2 mpirun mli-program`, or add `-with-pmi` to your configure flags. More info in the [Open MPI page of the users' wiki](../../wiki/Compile_your_own_Open_MPI)

  Wiki documentation
--------------------
The general documentation is embedded in this document. You may want to consider the supplementary Admin and User-generated documentation available in the [wiki](../../wiki/)

  See also
----------

+ Access
    + [account request form]
    + [ssh_config(5)]
    + [ssh-agent(1)]
    + [rsync(1)]
+ Software
    + [module(1)]
+ Job launch
    + [slurm-quickstart]
    + [slurm(1)]
    + [sinfo(1)]
    + [salloc(1)]
    + [sbatch(1)]
    + [ompi_info(1)]
    + [mpirun(1)]
+ Hardware
    + [Haswell E5-2650 v3]
    + [Westmere-EP E5606]
    + [Gainestown E5520]
    + [hwloc-ls(1)]
    + [nvidia-smi(1)]
    + [miccheck(1)]

[account request form]: https://admin.icl.utk.edu/acctmgr/apply.html
[ssh_config(5)]: https://man.openbsd.org/ssh_config
[ssh-agent(1)]: https://man.openbsd.org/ssh-agent
[rsync(1)]: https://download.samba.org/pub/rsync/rsync.html

[slurm-quickstart]: https://slurm.schedmd.com/quickstart.html
[slurm(1)]: https://slurm.schedmd.com/slurm.html
[sinfo(1)]: https://slurm.schedmd.com/sinfo.html
[salloc(1)]: https://slurm.schedmd.com/salloc.html
[sbatch(1)]: https://slurm.schedmd.com/sbatch.html
[ompi_info(1)]: https://www.open-mpi.org//doc/v2.1/man1/ompi_info.1.php
[mpirun(1)]: https://www.open-mpi.org//doc/v2.1/man1/mpirun.1.php

[module(1)]: http://modules.sourceforge.net/c/module.html

[Haswell E5-2650 v3]: http://ark.intel.com/products/81705/Intel-Xeon-Processor-E5-2650-v3-25M-Cache-2_30-GHz
[Westmere-EP E5606]: http://ark.intel.com/products/52583/Intel-Xeon-Processor-E5606-8M-Cache-2_13-GHz-4_80-GTs-Intel-QPI
[Gainestown E5520]: http://ark.intel.com/products/40200/Intel-Xeon-Processor-E5520-8M-Cache-2_26-GHz-5_86-GTs-Intel-QPI
[hwloc-ls(1)]: https://www.open-mpi.org/projects/hwloc/doc/v1.11.7/
[nvidia-smi(1)]: https://developer.nvidia.com/nvidia-system-management-interface
[miccheck(1)]: https://software.intel.com/en-us/articles/intel-manycore-platform-software-stack-mpss#moredoc

  Contact the administrators
----------------------------
If all else fails, please direct all your help and assistance requests to <mailto:iclhelp@icl.utk.edu>


CHANGELOG
=========

  Hardware upgrades
-------------------

+ May 2017
    + Compute nodes alembert received Infiniband upgrade (EDR)
    + Compute nodes bezout received Infiniband upgrade (FDR)

  Software upgrades
-------------------
+ November 2017
    + Intel Parallel XE 2018

+ June 2017
    + Open MPI 2.1.1
    + Knem 1.1.2
    + XPmem 2.6.3
    + UCX 1.2.0
    + PAPI 5.5.1 (w/msr-safe)

+ May 2017
    + Scientific Linux 7.3
    + Open MPI 2.0.2
    + MPSS 3.8
    + CUDA 8.0
    + gcc 4.8.5 (default), 4.9.4, 5.4.0, 6.3.0, 7.1.0
    + Intel icc 16.0.109
    + Score-P3 3.0
    + Scalasca 2.3.1
    + PAPI 5.5.0
    + MKL 2017.1


LEGAL
=====

  Terms of use
--------------
```
  ==============  saturn.icl.utk.edu - The University of Tennessee  ==============
   This is a  University of Tennessee computer system  and is for  authorized use
   only. Unauthorized or improper use of this system may result in administrative
   disciplinary  action  and/or civil charges/criminal  penalties. Users  have no
   expectation of privacy in any materials they place or view on this system. Any
   or all  uses of this system  and all files on this system  may be intercepted,
   monitored,  recorded, copied,  audited, inspected, and disclosed to authorized
   University and law enforcement personnel, as well as authorized individuals of
   other  organizations.   By continuing  to use this  system you  indicate  your
   awareness of and consent to these terms and conditions of use.
```

  Copyrights
------------
```
  1 Most files in this repository are marked with the copyrights of the
  2 organizations who have edited them.  The copyrights below are in no
  3 particular order and generally reflect members of the core
  4 team who have contributed code to this release.  The copyrights for
  5 code used under license from other parties are included in the
  6 corresponding files.
  7
  8 Copyright (c) 2017      The University of Tennessee and The University
  9                         of Tennessee Research Foundation.  All rights
 10                         reserved.
 11
 12 $COPYRIGHT$
 13
 14 Additional copyrights may follow
 15
 16 $HEADER$
 17
 18 Redistribution and use in source and binary forms, with or without
 19 modification, are permitted provided that the following conditions are
 20 met:
 21
 22 - Redistributions of source code must retain the above copyright
 23   notice, this list of conditions and the following disclaimer.
 24
 25 - Redistributions in binary form must reproduce the above copyright
 26   notice, this list of conditions and the following disclaimer listed
 27   in this license in the documentation and/or other materials
 28   provided with the distribution.
 29
 30 - Neither the name of the copyright holders nor the names of its
 31   contributors may be used to endorse or promote products derived from
 32   this software without specific prior written permission.
 33
 34 The copyright holders provide no reassurances that the source code
 35 provided does not infringe any patent, copyright, or any other
 36 intellectual property rights of third parties.  The copyright holders
 37 disclaim any liability to any recipient for claims brought against
 38 recipient by any third party for infringement of that parties
 39 intellectual property rights.
 40
 41 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 42 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 43 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 44 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 45 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 46 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 47 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 48 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 49 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 50 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 51 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```